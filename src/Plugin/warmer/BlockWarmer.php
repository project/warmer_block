<?php

namespace Drupal\warmer_block\Plugin\warmer;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\warmer\Plugin\WarmerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The cache warmer for the block render cache.
 *
 * @Warmer(
 *   id = "block",
 *   label = @Translation("Block"),
 *   description = @Translation("Loads the selected blocks to warm the render cache.")
 * )
 */
final class BlockWarmer extends WarmerPluginBase {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  private $blockManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  private $contextRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    assert($instance instanceof BlockWarmer);

    /** @var \Drupal\Core\Block\BlockManagerInterface $block_manager */
    $block_manager = $container->get('plugin.manager.block');
    $instance->setBlockManager($block_manager);

    /** @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository */
    $context_repository = $container->get('context.repository');
    $instance->setContextRepository($context_repository);

    return $instance;
  }

  /**
   * Injects the block manager.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   Block manager.
   */
  public function setBlockManager(BlockManagerInterface $block_manager) {
    $this->blockManager = $block_manager;
  }

  /**
   * Injects the context repository.
   *
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   Context repository.
   */
  public function setContextRepository(ContextRepositoryInterface $context_repository) {
    $this->contextRepository = $context_repository;
  }

  /**
   * The list of all item IDs for all blocks in the system.
   *
   * @var array
   */
  private $itemIds = [];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadMultiple(array $ids = []): array {
    $items = [];
    foreach ($ids as $id) {
      $items[] = $this->blockManager->createInstance($id);
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function warmMultiple(array $items = []) {
    foreach ($items as $block) {
      assert($block instanceof BlockPluginInterface);
      // Building the block will warm the cache.
      $block->build();
    }

    return count($items);
  }

  /**
   * {@inheritdoc}
   *
   * @todo This is a naive implementation.
   */
  public function buildIdsBatch($cursor): array {
    $configuration = $this->getConfiguration();
    if (empty($this->itemIds) && !empty($configuration['blocks'])) {
      $this->itemIds = array_values($configuration['blocks']);
    }
    $cursor_position = is_null($cursor) ? -1 : array_search($cursor, $this->itemIds);
    if ($cursor_position === FALSE) {
      return [];
    }
    return array_slice($this->itemIds, $cursor_position + 1, (int) $this->getBatchSize());
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreConfigurationFormElements(array $form, SubformStateInterface $form_state): array {
    $definitions = $this->blockManager->getDefinitionsForContexts($this->contextRepository->getAvailableContexts());

    $options = [];
    foreach ($definitions as $id => $block) {
      $options[strval($block['category'])][$id] = $block['admin_label'];
    }

    $configuration = $this->getConfiguration();
    $form['blocks'] = [
      '#type' => 'select',
      '#title' => $this->t('Blocks'),
      '#description' => $this->t('Enable the blocks to warm asynchronously.'),
      '#options' => $options,
      '#default_value' => empty($configuration['blocks']) ? [] : $configuration['blocks'],
      '#multiple' => TRUE,
      '#attributes' => ['style' => 'min-height: 60em;'],
    ];

    return $form;
  }

}
