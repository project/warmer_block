Block warmer
===================

INTRODUCTION
------------

This module adds a new option to [Warmer](https://www.drupal.org/project/warmer)
that allows warming the render cache for a specific block.
It is useful when you have a block that is slow to render.

Note that it only warms the cache for the current context
(if run with Drush that would be the default language, anonymous user, etc.).


REQUIREMENTS
------------

 * [Warmer](https://www.drupal.org/project/warmer)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   <https://www.drupal.org/documentation/install/modules-themes/modules-8>
   for further information.


CONFIGURATION
-------------

This module can be configured like any Warmer module on
`/admin/config/development/warmer/settings`.

You can choose which blocks to warm and how often to run the warmer.


MAINTAINERS
-----------

Current maintainers:

 * Pierre Rudloff (prudloff) - <https://www.drupal.org/u/prudloff>

This project has been sponsored by:

 * Insite:
    A lasting and independent business project
    with more than 20 years experience
    and several hundred references in web projects and visual communication
    with public actors, associative and professional networks.
    Visit <https://www.insite.coop/> for more information.
